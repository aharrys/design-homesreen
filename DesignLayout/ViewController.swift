//
//  ViewController.swift
//  DesignLayout
//
//  Created by Akhmad Harry Susanto on 09/02/20.
//  Copyright © 2020 Akhmad Harry Susanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewBlue: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewBlue.roundedButtom()
    }
    

}

extension UIView {
    func roundedButtom () {
        let maskPath = UIBezierPath (roundedRect: bounds, byRoundingCorners: [.bottomRight, .bottomLeft], cornerRadii: CGSize(width: 20, height: 20))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.path = maskPath.cgPath
        layer.mask = maskLayer1
    }
}

